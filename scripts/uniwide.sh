#!/bin/bash

if [ "$(id -u)" != "0" ]; then
    echo "Must be root!" 1>&2
    exit 1
fi

ip link set wlp4s0 up
sleep 0.5
ip link set wlp4s0 down
sleep 0.5
netctl start uniwide
