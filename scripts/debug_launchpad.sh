#/bin/bash
source ~/dev/echronos-core/setenv
arm-none-eabi-gdb $1\
 -ex "target remote localhost:3333"\
 -ex "monitor reset halt"\
 -ex "monitor flash write_image erase $1"\
 -ex "monitor reset halt"\
 -ex "monitor arm semihosting enable"
